<?php

namespace Drupal\taxonomy_term_import_with_translations\Batch;

/**
 * @file
 * Contains \Drupal\taxonomy_term_import_with_translations\Batch.
 */

use Drupal\taxonomy\Entity\Term;

/**
 * Class XlsxBatchImport Import taxonomy terms and translations.
 */
class XlsxBatchImport {

  /**
   * The current data to be perform.
   *
   * @param array $data
   */

  /**
   * Import taxonomy terms and translations.
   */
  public static function importTaxonomyTerms(array $data, &$context) {
    if (!empty($data)) {
      $vid = $data['vid'];
      $enable_languages = $data['languages'];

      $terms_data = $data['term'];
      if (!empty($terms_data)) {
        $termStorage = \Drupal::entityTypeManager();
        $entityRepository = \Drupal::service('entity.repository');
        foreach ($terms_data as $key => $term_data) {
          if (empty($term_data)) {
            continue;
          }
          if ($key == 'en') {
            $term_load = $termStorage->getStorage('taxonomy_term')->loadByProperties([
              'name' => $term_data,
              'vid' => $vid,
            ]);

            if (empty($term_load)) {
              $term = Term::create([
                'name' => $term_data,
                'vid' => $vid,
              ]);
              $term->save();
            }
          }

          else {
            if (!empty($term_data)) {
              foreach ($term_data as $trans_key => $translation) {
                if (empty($translation)) {
                  continue;
                }
                $term_load = $termStorage->getStorage('taxonomy_term')->loadByProperties([
                  'name' => $terms_data['en'],
                  'vid' => $vid,
                ]);
                if (!empty($term_load)) {
                  $term = end($term_load);
                  if (!empty($term)) {
                    if (!$term->hasTranslation($trans_key)) {
                      if (!empty($trans_key)) {
                        $term->addTranslation($trans_key, ['name' => $translation]);
                        $term->save();
                      }
                    }
                    else {
                      if (!empty($translation) && !empty($trans_key)) {
                        $translated_node = $entityRepository->getTranslationFromContext($term, $trans_key);
                        $translated_node->setName($translation);
                        $term->save();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Batch Process completion.
   */
  public static function importCompleted($success, $results, $operations) {
    $message = 'Taxonomy terms imported successfully';
    \Drupal::messenger()->addMessage($message);
  }

}
