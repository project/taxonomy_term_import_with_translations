<?php

namespace Drupal\taxonomy_term_import_with_translations\Form;

/**
 * @file
 * Contains \Drupal\taxonomy_term_import_with_translations\Form\ImportForm.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;

use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Contribute form.
 */
class ImportForm extends FormBase {

  /**
   * The masanger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, Connection $database, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->database = $database;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('database'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_terms_xlsx';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $list_vocabularies = [];

    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    if (!empty($vocabularies)) {
      foreach ($vocabularies as $key => $vocabulary) {
        $list_vocabularies[$key] = $vocabulary->get('name');
      }
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#required' => TRUE,
      '#description' => $this->t('Select the vocabularies you would like to import'),
      '#options' => $list_vocabularies,
    ];
    $form['upload_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Import file'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['xlsx'],
        'file_validate_size' => [25600000],
      ],
      '#upload_location' => 'public://terms-xlsx/',
      '#description' => $this->t('Upload a xlsx file to Import data in selected vocabulary.'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get all enabled languages.
    $languages = \Drupal::languageManager()->getLanguages(LanguageInterface::STATE_CONFIGURABLE);
    $list_languages = [];

    if (!empty($languages)) {
      foreach ($languages as $language) {
        $list_languages[$language->getId()] = $language->getName();
      }
    }
    $vid = $form_state->getValue('vocabulary');

    $xlsx_data = $this->importTermsData($form_state, $list_languages);
    $term['data'] = $xlsx_data;
    if (count($xlsx_data) > 0) {
      foreach ($term['data'] as $value) {
        $data = [
          'term' => $value,
          'vid' => $vid,
          'languages' => $list_languages,
        ];
        $operations[] =
         ['\Drupal\taxonomy_term_import_with_translations\Batch\XlsxBatchImport::importTaxonomyTerms',
          [$data],
         ];
      }
      $batch = [
        'title' => $this->t('Import taxonomy terms'),
        'operations' => $operations,
        'init_message' => $this->t('Batch is starting.'),
        'progress_message' => $this->t('Processed @current out of @total.'),
        'error_message' => $this->t('Batch has encountered an error.'),
        'finished' => '\Drupal\taxonomy_term_import_with_translations\Batch\XlsxBatchImport::importCompleted',
      ];
      batch_set($batch);
    }
  }

  /**
   * Function to implement import m49 data functionality.
   */
  public function importTermsData(FormStateInterface $form_state, $languages) {
    // Get uploaded file name.
    $file = $this->entityTypeManager->getStorage('file')
      ->load($form_state->getValue('upload_file')[0]);
    $full_path = $file->get('uri')->value;
    $file_name = basename($full_path);
    $file_extension = pathinfo($file_name);

    $row_data = [];
    $inputFileName = \Drupal::service('file_system')->realpath('public://terms-xlsx/' . $file_name);
    $this->selectedFileDetails = [
      'filename' => $file_name,
      'fileurl' => $full_path,
    ];

    if ($file_extension['extension'] == 'xlsx') {
      $spreadsheet = IOFactory::load($inputFileName);
      $sheetData = $spreadsheet->getActiveSheet();
      $row_key = [];
      $i = 0;
      foreach ($sheetData->getRowIterator() as $column => $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(FALSE);
        $cells = [];
        $column_count_iterator = 0;
        $atleast_one_column_data = 0;
        foreach ($cellIterator as $key => $cell) {
          $atleast_one_column_data++;
          $name = $cell->getCalculatedValue();
          $col_key = array_search($name, $languages);
          if (empty($name)) {
            continue;
          }
          if ($column === 1) {
            $row_key[$key] = $col_key;
          }
          else {
            if ($row_key[$key] == 'en') {
              $cells[$row_key[$key]] = $cell->getCalculatedValue();
            }
            else {
              $cells['translation'][$row_key[$key]] = $cell->getCalculatedValue();
            }
            $i++;
          }

          $column_count_iterator++;
        }

        if (!empty($cells) && $atleast_one_column_data) {
          $row_data[] = $cells;
        }

      }
    }
    else {
      $this->messenger->addError($this->t('Incorrect file type. Allowed file extensions are .xlsx'));
    }

    if (count($row_data) < 2) {
      $this->messenger->addError($this->t('File contains no data'));
    }
    return $row_data;
  }

}
