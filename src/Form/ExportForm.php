<?php

namespace Drupal\taxonomy_term_import_with_translations\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Form for the Gettext translation files export form.
 *
 * @internal
 */
class ExportForm extends FormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;


  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ExportForm.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(LanguageManagerInterface $language_manager, FileSystemInterface $file_system, Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->languageManager = $language_manager;
    $this->fileSystem = $file_system;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('file_system'),
      $container->get('database'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_terms_xlsx';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $list_vocabularies = [];

    if (!empty($vocabularies)) {
      foreach ($vocabularies as $key => $vocabulary) {
        $list_vocabularies[$key] = $vocabulary->get('name');
      }
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#required' => TRUE,
      '#description' => $this->t('Select the vocabularies you would like to import'),
      '#options' => $list_vocabularies,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $selected_vocabulary = $form_state->getValue('vocabulary');
    if (!empty($selected_vocabulary)) {
      $entityRepository = \Drupal::service('entity.repository');
      $languages = \Drupal::languageManager()->getLanguages(LanguageInterface::STATE_CONFIGURABLE);
      $list_languages = [];

      if (!empty($languages)) {
        foreach ($languages as $language) {
          $list_languages[$language->getId()] = $language->getName();
        }
      }
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
        'vid' => $selected_vocabulary,
      ]);
      if (!empty($terms)) {
        $terms_data = [];
        foreach ($terms as $term) {

          // Get terms with translations.
          $translated_term = [];
          foreach ($list_languages as $language_key => $language) {
            if ($language_key == 'en') {
              $translated_term[$language] = $term->getName();
            }
            else {
              if ($term->hasTranslation($language_key)) {
                $translation = $entityRepository->getTranslationFromContext($term, $language_key);
                $translated_term[$language] = $translation->getName();
              }
              else {
                $translated_term[$language] = '';
              }
            }
          }
          $terms_data[] = $translated_term;
        }
      }
    }

    // Name for Excel file.
    $file = $selected_vocabulary . date('dmY') . ".xlsx";

    // Headers to force download Excel file.
    header("Content-Disposition: attachment; filename=\"$file\"");
    header("Content-Type: application/vnd.ms-excel");

    $flag = FALSE;
    foreach ($terms_data as $row) {
      if (!$flag) {
        // Set column names as first row.
        echo implode("\t", array_keys($row)) . "\n";
        $flag = TRUE;
      }

      // Filter data.
      $row = $this->filterData($row);
      echo implode("\t", array_values($row)) . "\n";
    }
    exit;
  }

  /**
   * Function to filter data.
   */
  public function filterData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"')) {
      $str = '"' . str_replace('"', '""', $str) . '"';
    }
    return $str;
  }

}
